[1] Anagnostopoulos, D., Rutledge, S.A. & Jacobsen, R. 2013. The Infrastructure of Accountability: Data use and the transformation of American education.

[2] Beer, D., 2018. *The data gaze: Capitalism, power and perception*. Sage.

[3] Beer, D., 2018. Envisioning the power of data analytics. *Information, Communication & Society*, *21*(3), pp.465-479.

[4] Couldry, N. and Mejias, U.A., 2019. The Costs of Connection: How Data Are Colonizing Human Life and Appropriating It for Capitalism.

[5] Couldry, N. and Mejias, U.A., 2019. Data colonialism: Rethinking big data’s relation to the contemporary subject. *Television & New Media*, *20*(4), pp.336-349.

[6] Fontaine, C. 2016. The Myth of Accountability: How Data (Mis)Use is Reinforcing the Problems of Public Education, Data and Society Working Paper 08.08.2016.

[7] Gitelman, L. ed., 2013. *Raw data is an oxymoron*. MIT press.

[8] McGuffin D (2020) Plans to Develop High-Tech 'Smart City' In Toronto met with resistance. Available from: https://www.npr.org/2020/02/16/806417352/plans-to-develop-high-tech-smart-city-in-toronto-met-with-resistance 

[9] Kitchin, R., 2014. *The data revolution: Big data, open data, data infrastructures and their consequences*. Sage.

[10] Knox, J., Williamson, B. and Bayne, S., 2020. Machine behaviourism: Future visions of ‘learnification’ and ‘datafication’ across humans and digital technologies. *Learning, Media and Technology*, *45*(1), pp.31-45.

[11] Michael, M. and Lupton, D., 2016. Toward a manifesto for the ‘public understanding of big data’. Public Understanding of Science, 25(1), pp.104-116.

[12] Perrotta, C. and Selwyn, N., 2020. Deep learning goes to school: Toward a relational understanding of AI in education. *Learning, Media and Technology*, *45*(3), pp.251-269.

[13] Pihama, L. and Lee-Morgan, J., 2018. Colonization, education, and Indigenous peoples. *Indigenous handbook of education*, pp.19-27.

[14] Raffaghelli, J.E. & Stewart, B. 2020. Centering complexity in ‘educators’ data literacy’ to support future practices in faculty development: a systematic review of the literature, Teaching in Higher Education, 25:4, 435-455, DOI: 10.1080/13562517.2019.1696301

[15] Selwyn, N., 2016. *Is technology good for education?*. John Wiley & Sons

[16] Stein, S. and de Oliveira Andreotti, V., 2017. Higher education and the modern/colonial global imaginary. *Cultural Studies↔Critical Methodologies*, *17*(3), pp.173-181.

[17] Tuck, E. and Yang, K.W., 2012. Decolonization is not a metaphor. *Decolonization: Indigeneity, education & society*, *1*(1).

[18] van Dijck, J., 2014. Datafication, dataism and dataveillance: Big Data between scientific paradigm and ideology. *Surveillance & society*, *12*(2), pp.197-208.

[19] van Dijck, J., Poell, T. and De Waal, M., 2018. *The platform society: Public values in a connective world*. Oxford University Press.

[20] Weller, M., 2015. MOOCs and the Silicon Valley narrative. *Journal of Interactive Media in Education*, *2015*(1), pp.1-7.

[21] Williamson, B., 2017. Big data in education: The digital future of learning, policy and practice. Sage.

[22] Williamson, B., 2018. The hidden architecture of higher education: Building a big data infrastructure for the ‘smarter university’. *International Journal of Educational Technology in Higher Education*, *15*(1), pp.1-26.

[23] Williamson, B., 2021. Making markets through digital platforms: Pearson, edu-business, and the (e) valuation of higher education. *Critical Studies in Education*, *62*(1), pp.50-66.

[24] Woetzel J, Remes J, Boland B, et al. (2018) Smart cities: Digital solutions for a more livable future. *McKinsey & Company*, McKinsey & Company. Available from: https://www.mckinsey.com/business-functions/operations/our-insights/smart-cities-digital-solutions-for-a-more-livable-future. 